from flask import Flask, request, jsonify
from flask_cors import CORS
import mysql.connector
import json
import os

app = Flask(__name__)
cors = CORS(app)

# searchpages
@app.route('/')
def main():

    web = request.args.get('web')
    f = open("sites.txt","w")
    f.write(str(web))
    f.close

    return web

@app.route('/go')
def go():
    os.system('python3 go-spider.py')

    return ("Webswinging complete")

@app.route('/test')
def test():
    return("hello")

if __name__ == '__main__':
    app.run(host='0.0.0.0', port = '5002')
