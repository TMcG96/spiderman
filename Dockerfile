FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y python3-pip python3-dev

WORKDIR /usr/src/app

RUN pip3 install -U Flask
RUN pip3 install -U scrapy
RUN pip3 install -U flask_cors
RUN pip3 install -U mysql-connector
RUN pip3 install requests
RUN pip3 install -U beautifulsoup4

COPY . .

CMD ["python3", "./webselect.py"]
