from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from peter.spiders.peterparker import PeterparkerSpider

process = CrawlerProcess(get_project_settings())
process.crawl(PeterparkerSpider)
process.start()
