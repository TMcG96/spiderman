from bs4 import BeautifulSoup
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from peter.items import PeterItem
from datetime import datetime

class PeterparkerSpider(CrawlSpider):
    name = 'peterparker'

    f = open("sites.txt","r")
    site = f.read().splitlines()
    start = 'https://www.'+site[0]+'/'

    print(site)
    print(start)

    allowed_domains = [site[0]]
    start_urls = [start]

    rules = (Rule(LinkExtractor(tags='a',attrs='href',unique=True),callback='parse_item',follow=True),)

    def parse_item(self, response):
        soup = BeautifulSoup(response.body)
        for script in soup(["script","style"]):
            script.extract()
        pageText = str(soup.get_text()).replace('\n','')
        lines = (line.strip() for line in pageText.splitlines())
        chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
        pageText = '\n'.join(chunk for chunk in chunks if chunk)
        item=PeterItem()
        item['url'] = response.request.url
        item['date'] = str(datetime.now().strftime("%Y/%m/%d %H:%M:%S"))
        item['page'] = pageText

        return item
        # yield {
        #     'url': response.request.url,
        #     'date': date.today(),
        #     'page': str(soup.get_text()).replace('\n',''),
        # }
