# -*- coding: utf-8 -*-
import mysql.connector
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


class PeterPipeline(object):
    def process_item(self, item, spider):
        mydb = mysql.connector.connect(
            host='10.102.192.3',
            user='root',
            passwd='tommyuzz',
            database='Websites'
        )

        mycursor = mydb.cursor()
        sql = "INSERT INTO page (uri, indexedat, content) VALUES (%s,DATE(%s),%s)"
        val = (
            item['url'],
            item['date'],
            item['page']
            )

        mycursor.execute(sql,val)
        mydb.commit()
