# -*- coding: utf-8 -*-

# Define here the models for your scraped items
from scrapy.item import Item, Field
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class PeterItem(scrapy.Item):
    url = Field()
    date = Field()
    page = Field()
